# SGBD

## In pincipiu toate informatiile sunt in [Cursuri](./Cursuri).

1. [Lab 1](./Laboratoare/lab1) se face cu un tool de Java, deci trebuie instalat Java si rulata comanda in terminal/cmd in direcotrul in care se afla fisierul `ra.jar`. Optiunea `-h` aduce meniul de help. Optiunea `-i` ruleaza query-urile dintr-un fisier extern:

`> java -jar ra.jar -h`

`> java -jar ra.jar -i pizza.ra`

2. [Lab 2](./Laboratoare/lab2) se face intr-o masina virtuala. Pentru asta instalati VirtualBox si creati o masina virtuala folosind fisierul Fedora_x64.vdi ce se gaseste pe site-ul profului [http://hash.atspace.eu/dbms/](http://hash.atspace.eu/dbms/).

![masina virtuala](./Laboratoare/lab2/masina_virtuala.jpg)

Acolo sunt instalate si Python pentru scripturi si MySql pentru query-uri. (De retinut ca se poate realiza si pe calculatorul personal in MySql fara masina virtuala, dar nu stiu cum trebuie modificate scripturile de Python sa functioneze asa)

3. [Lab 3](./Laboratoare/lab3) si [Lab 4](./Laboratoare/lab4) ar trebui rezolvate tot pe masina virtuala, dar se pot face si in MySql pe calculatorul personal.