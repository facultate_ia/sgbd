use userdb;

-- Exercitiul 1
-- Adaugati reviewer-ul ’Roger Ebert’, cu rID-ul 209.
insert into Reviewer 
	values(209, 'Roger Ebert');
-- Query de verificare
select * from Reviewer order by rID, name;


-- Exercitiul 2
-- Pentru reviewer-ul ’James Cameron’ introduceti rating-uri de 5 stele pentru toate filmele din DB.
-- Data review-ului va fi NULL.
insert into Rating 
	select (select rID from Reviewer where Reviewer.name = 'James Cameron'), mID, 5, NULL from Movie;
-- Query de verificare
select * from Rating where stars = 5 order by rID, mID;


-- Exercitiul 3
-- Pentru toate filmele care au un rating mediu de 4 stele si peste, adaugati 25 la anul release-ului. 
-- (Nu adaugati noi tuple, ci actualizati tuplele existente).
drop table if exists temp;
create table temp select Rating.mID
from Rating
group by Rating.mID
having avg(stars) >= 4;
update Movie
set Movie.year = Movie.year + 25
where Movie.mID in (
	select mID
    from temp
);

-- Error Code: 1175. You are using safe update mode and you tried to update a table without a WHERE that
-- uses a KEY column.  To disable safe mode, toggle the option in Preferences -> SQL Editor and reconnect.

-- Query de verificare
select * from Movie order by mID;


-- Exercitiul 4
-- Stergeti toate rating-urile pentru care anul filmului este inainte de 1970 sau dupa 2000, 
-- iar rating-ul este mai putin de 4 stele.
drop table if exists temp;
create table temp
	select Rating.mID, Rating.rID, Rating.stars
	from Rating
    join Movie on Rating.mID = Movie.mID
	where (Movie.year < 1970 or Movie.year > 2000) and Rating.stars < 4;
delete from Rating
where mID in (select mID from temp) and rID in (select rID from temp) and stars in (select stars from temp);
drop table if exists temp;
-- Query de verificare
select R.rID, R.mID, R.stars, M.title, M.year from Rating R join Movie M on (R.mID = M.mID) order by R.rID, R.mID;

-- Exercitiul 5
-- Pentru fiecare situatie pentru care elevul A il place pe elevul B, dar elevul B place 
-- insa un alt elev (diferit) C, afisati numele si notele lui A, B si C.
-- Raspuns:
-- Andrew 10 Cassandra 9 Gabriel 9 
-- Gabriel 11 Alexis 11 Kris 10
select A.name, A.grade , B.name, B.grade, C.name, C.grade from Highschooler as A
join Likes on A.ID = Likes.ID1
join Highschooler as B on Likes.ID2 = B.ID
join Likes as lk1 on lk1.ID1 = B.ID and lk1.ID2 != A.ID
join Highschooler as C on lk1.ID2 = C.ID;


-- Exercitiul 6
-- Gasiti acei elevi care au toti prietenii din clase diferite fata de a lor. 
-- Tipariti numele si notele acestor elevi.
-- Raspuns: 
-- Austin 11
select hs.name, hs.grade
from (
	select distinct Highschooler.ID, Highschooler.name, Highschooler.grade
	from Highschooler
	join Friend on Highschooler.ID = Friend.ID1
	join Highschooler as hs1 on Friend.ID2 = hs1.ID
	where Highschooler.grade != hs1.grade
) as hs
where hs.ID not in (
	select distinct Highschooler.ID
	from Highschooler
	join Friend on Highschooler.ID = Friend.ID1
	join Highschooler as hs1 on Friend.ID2 = hs1.ID
	where Highschooler.grade = hs1.grade
);


-- Exercitiul 7
-- Care este numarul mediu de prieteni pe elev?
-- Raspuns: 2.5
select round((
	select sum(nFriendsHs)
	from (
		select count(Friend.ID2) as nFriendsHs
		from Highschooler
		join Friend on Highschooler.ID = Friend.ID1
		group by Highschooler.ID
    ) as nFriends
) / count(Highschooler.ID), 1)
from Highschooler;


-- Exercitiul 8
-- Gasiti numarul de elevi care fie sunt prieteni cu Cassandra fie sunt prieteni ai prietenilor directi ai Cassandrei.
-- Nu o includeti si pe Cassandra, desi tehnic ea este un prieten al unui prieten al ei.
-- Raspuns: 7
select count(ID)
from ((
		select hs1.ID
		from Highschooler
		join Friend as fr1 on Highschooler.ID = fr1.ID1
		join Highschooler as hs1 on fr1.ID2 = hs1.ID
		where Highschooler.name = 'Cassandra'
    ) union (
		select hs2.ID
		from Highschooler
		join Friend as fr1 on Highschooler.ID = fr1.ID1
		join Highschooler as hs1 on fr1.ID2 = hs1.ID
		join Friend as fr2 on hs1.ID = fr2.ID1
		join Highschooler as hs2 on fr2.ID2 = hs2.ID and hs2.name != 'Cassandra'
		where Highschooler.name = 'Cassandra'
)) as Friends;

-- Exercitiul 9
-- Gasiti numele si nota elevilor care au cel mai mare numar de prieteni. 
-- Raspuns:
-- Alexis 11
-- Andrew 10
select hs.name, hs.grade
from (
	select Highschooler.name, Highschooler.grade, count(Friend.ID2) as nFriendsHs
	from Highschooler
	join Friend on Highschooler.ID = Friend.ID1
	group by Highschooler.ID, Highschooler.name, Highschooler.grade
) as hs
where hs.nFriendsHs = (
	select max(nFriendsHs) as maxim
	from (
		select count(Friend.ID2) as nFriendsHs
		from Highschooler
		join Friend on Highschooler.ID = Friend.ID1
		group by Highschooler.ID, Highschooler.name
	) as nFriendsTbl)
order by hs.name, hs.grade;
