use userdb;

-- Exercitiul 1
-- Gasiti numele tuturor elevilor care sunt prieteni cu cineva numit Gabriel.
select Highschooler.name
from Highschooler
join Friend on Highschooler.ID = Friend.ID1
join Highschooler as hs1 on Friend.ID2 = hs1.ID
where hs1.name = 'Gabriel'
order by Highschooler.name;


-- Exercitiul 2
-- Pentru fiecare elev care place un alt elev cu doua sau mai multe clase mai mic,
-- returnati numele si clasa (grade) acelui elev, si numele si clasa elevului pe care il place.
select Highschooler.name, Highschooler.grade, hs1.name, hs1.grade
from Highschooler
join Likes on Highschooler.ID = Likes.ID1
join Highschooler as hs1 on Likes.ID2 = hs1.ID
where Highschooler.grade - hs1.grade >= 2;


-- Exercitiul 3
-- Pentru fiecare pereche de elevi care se plac reciproc, returnati numele si clasa ambilor elevi.
-- Includeti fiecare pereche doar o data, cu cele doua nume in ordine alfabetica.
select distinct hs1.name, hs1.grade, hs2.name, hs2.grade
from Highschooler as hs1
join Likes on hs1.ID = Likes.ID1
join Highschooler as hs2 on Likes.ID2 = hs2.ID
join Likes as lk1 on hs2.ID = lk1.ID1 and hs1.ID = lk1.ID2
where hs1.name < hs2.name
order by hs1.name, hs2.name;


-- Exercitiul 4
-- Gasiti toti elevii care nu apar in relatia ’Likes’ (ca elev care place sau este placut) si
-- intoarceti numele si clasele lor. Sortati dupa clasa, apoi dupa nume in cadrul fiecarei clase.
select Highschooler.name, Highschooler.grade
from Highschooler
where Highschooler.ID not in (
	select Likes.ID1
    from Likes
) and Highschooler.ID not in (
	select Likes.ID2
    from Likes
)
order by Highschooler.grade, Highschooler.name;


-- Exercitiul 5
-- Pentru fiecare situatie in care elevul A il place pe elevul B, insa pentru B nu avem vreo
-- informatie cu privire la cel pe care il place acesta (adica B nu apare ca ID1 in tabela ’Likes’),
-- returnati numele si clasa lui A si lui B.
select Highschooler.name, Highschooler.grade, hs1.name, hs1.grade
from Highschooler
join Likes on Highschooler.ID = Likes.ID1
join Highschooler as hs1 on Likes.ID2 = hs1.ID
where hs1.ID not in (
	select Likes.ID1
    from Likes
)
order by Highschooler.name;


-- Exercitiul 6
-- Gasiti numele si clasele elevilor care au prieteni doar in aceeasi clasa (grade).
-- Intoarceti rezultatul sortat dupa clasa, apoi dupa nume in cadrul fiecarei clase.
select Highschooler.name, Highschooler.grade
from Highschooler
where Highschooler.ID not in
(
	select Highschooler.ID
	from Highschooler
	join Friend on Highschooler.ID = Friend.ID1
	join Highschooler as hs1 on Friend.ID2 = hs1.ID
	where Highschooler.grade != hs1.grade
)
order by Highschooler.grade, Highschooler.name;


-- Exercitiul 7
-- Pentru fiecare elev A care place un elev B pentru care cei doi nu sunt prieteni, gasiti
-- daca ei au un prieten comun C (care le poate face cunostinta). Pentru toate triplele astfel gasite,
-- returnati numele si clasa pentru A, B si C.
select distinct A.name, A.grade, B.name, B.grade, C.name, C.grade
from Highschooler as A
join Likes on A.ID = Likes.ID1
join Highschooler as B on Likes.ID2 = B.ID
join Friend as frd1 on A.ID = frd1.ID1
join Highschooler as C on frd1.ID2 = C.ID
join Friend as frd2 on C.ID = frd2.ID1 and frd2.ID2 = B.ID
where A.ID not in (
	select A.ID
	from Highschooler as A
	join Likes on A.ID = Likes.ID1
	join Highschooler as B on Likes.ID2 = B.ID
	join Friend on A.ID = Friend.ID1 and B.ID = Friend.ID2
);


-- Exercitiul 8
-- Gasiti diferenta dintre numarul de elevi din scoala si numarul de nume diferite.
select count(name) - count(distinct name) as difference
from Highschooler;


-- Exercitiul 9
-- Gasiti numele si clasa tuturor elevilor care sunt placuti de mai mult de un elev.
select Highschooler.name, Highschooler.grade
from Highschooler
where (
	select count(Likes.ID1)
    from Likes
    where Likes.ID2 = Highschooler.ID
) > 1;
